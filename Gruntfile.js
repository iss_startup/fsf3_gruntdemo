module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {

      js: {
        src: ['source/js/**/*.js'],
        dest: 'build/js/main-concat.js'
      },
      templates: {
        src: ['source/templates/**/*.html'],
        dest: 'build/templates/templates.html'
      },
      css: {
        src: ['source/css/**/*.css'],
        dest: 'build/css/main.css'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        mangle: true,
        sourceMap: true,
        mangleProperties: true,
        compress: {
          drop_console: true,
          screw_ie8: true,
          warnings: false
        },
        report: 'gzip'
      },
      build: {
        src: 'build/js/main-concat.js',
        dest: 'build/js/main.min.js'
      }
    },
    less: {
      production: {
        files: {
          'source/css/main.css': 'source/css/sample.less'
        }
      }
    },
    cssmin: {
      production: {
        files: {
          'build/css/main.min.css' : ['build/css/main.css']
        }
      }
    },
    jshint: {
      allJS: ['source/js/**/*.js']
    },
    watch: {
      js: {
        files: ['source/js/**/*.js'],
        tasks: ['jshint']
      },
      less: {
        files: ['source/css/**/*.less'],
        tasks: ['less']
      }
    },
    clean: {
      reset: ['build', 'node_modules']
    },
    fileblocks: {
      options: {
        rebuild: true,
        removeFiles: true
      },
      dev: {
        src: 'source/index.html',
        blocks: {
          developmentJS: {
            cwd: 'source',
            src: [
                'js/*.js'
            ]
          }
        }
      },
      production: {
        src: 'build/index.html',
        blocks: {
          productionJS: {
            cwd: 'build',
            src: [
                'js/*.min.js'
            ]
          }
        }

      }
    },
    copy: {
      devIndex: {
        src: 'source/index.template.html',
        dest: 'source/index.html'
      },
      buildIndex: {
        src: 'source/index.template.html',
        dest: 'build/index.html'
      }
    },
    pngmin: {
      options: {
        ext: '.min.png'
      },
      devimages: {
        files: [
          {
            src: [
              'source/images/*.png',
              '!source/images/*.min.png'
            ],
            dest: 'source/images/'
          }
        ]
      },
      buildimages: {
        files: [
          {
            src: [
                'source/images/*.png',
                '!source/images/*.min.png'
            ],
            dest: 'build/images/'
          }
        ]
      }

    },
    aws_s3: {
      options: {
        accessKeyId: process.env.AWSKEYID,
        secretAccessKey: process.env.AWSSECRET,
        region: 'ap-southeast-1'
      },
      production: {
        options: {
          bucket: '/gruntdemo'
        },
        files: [
          {expand: true, cwd: 'build/', src: ['**'], dest: '/'},
        ]
      }
    }

  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-file-blocks');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-pngmin');
  grunt.loadNpmTasks('grunt-aws-s3')

  // Default task(s).
  grunt.registerTask('default', ['copy','jshint','less','concat','uglify','cssmin', 'fileblocks', 'pngmin', 'aws_s3']);
  grunt.registerTask('cssonly', ['less', 'concat:css','cssmin']);


};
