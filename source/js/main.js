
var someVar1 = 123;
var someOtherVar = 456;

function helloWorld() {
  var myInternalVar = 0;
  var myOtherInternalVar = 1;
  return 'HelloWorld' + (someVar1 + someOtherVar + myInternalVar + myOtherInternalVar);
}

var myObject = {};
myObject.someProperty = 'foobar';
myObject.someOtherProperty = 'barfoo';

var someUnusedVar = 'foo';

function helloUniverse() {}
function helloStackup() {}

console.log('hello world');


